"""
The functions in this module are copied from the Sunode library in order to remove the
dependency on sunode. The main purpose of sunode is to provide a wrapper for the
SUNDIALS library of ODE solvers – this is far beyond my needs in this project. Sunode
is licensed under an MIT license (smae as this project) – see the copyright notice and
full license text below.

Link to the sunode project: https://github.com/pymc-devs/sunode

=======================================================================================
MIT License

Copyright (c) 2020 Adrian Seyboldt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
=======================================================================================
"""

from typing import List, Tuple, Dict, Union, Any, Optional, Callable


def as_flattened(
    vals: Dict[str, Any], base: Optional[Tuple[str, ...]] = None
) -> Dict[Tuple[str, ...], Any]:
    if base is None:
        base = tuple()
    out = {}
    for name, val in vals.items():
        if isinstance(val, dict):
            flat = as_flattened(val, base=base + (name,))
            out.update(flat)
        else:
            out[base + (name,)] = val
    return out


def as_nested(vals: Dict[Tuple[str, ...], Any]) -> Dict[str, Any]:
    out: Dict[str, Any] = {}

    for names, val in vals.items():
        assert len(names) >= 1
        current = out
        for name in names[:-1]:
            current = current.setdefault(name, {})
        assert names[-1] not in current
        current[names[-1]] = val
    return out
