import dataclasses
import functools
import operator

from nesttool.convert import as_flattened, as_nested

def getitem_nested(dictionary, keylist):
    """Get item in nested dict providing a list of keys"""
    return functools.reduce(operator.getitem, keylist, dictionary)


def setitem_nested(dictionary, keylist, val):
    """Set item in nested dictionary"""
    getitem_nested(dictionary, keylist[:-1])[keylist[-1]] = val
    return dictionary


def getattr_nested(obj, attrs):
    """Get attribute of nested object.

    Parameters:
    ----------
    obj: object
        Nested object to get the attribute from.
    attrs: tuple of strings
        Tuple containing the names of the nested attributes.
    """
    return functools.reduce(getattr, (obj, *attrs))


def extract_subdict(nested_dict, subkeys):
    """Extract subdict from nested dict.

    Parameters:
    -----------
    nested_dict: dict
        Nested dictionary to extract a
    subkeys: list of tuple
        List of paths to extract from the dict
    """
    flat_subdict = {keys: getitem_nested(nested_dict, keys) for keys in subkeys}
    return sunode.dtypesubset.as_nested(flat_subdict)


def _is_namedtuple(x):
    return hasattr(x, "_asdict") and hasattr(x, "_make")


def apply_operator_nested(vals, factors, operator):
    "Apply an operator to a nested dict/dataclass/namedtuple"
    # Convert vals to dict
    is_dataclass = dataclasses.is_dataclass(vals)
    is_namedtuple = _is_namedtuple(vals)
    if is_dataclass:
        class_ = type(vals)
        vals = {
            field.name: getattr(vals, field.name) for field in dataclasses.fields(vals)
        }
    elif is_namedtuple:
        class_ = type(vals)
        vals = vals._asdict()

    # Convert factors to dict
    if dataclasses.is_dataclass(factors):
        factors = {
            field.name: getattr(factors, field.name)
            for field in dataclasses.fields(factors)
        }
    if hasattr(factors, "_asdict"):
        factors = factors._asdict()

    result = {}

    for name in list(vals):
        factor = factors[name]
        val = vals[name]
        if (
            isinstance(val, dict)
            or dataclasses.is_dataclass(val)
            or _is_namedtuple(val)
        ):
            result[name] = apply_operator_nested(val, factor, operator)
            continue

        if isinstance(val, tuple):
            val, shape = val
            result[name] = (operator(val, factor), shape)
        else:
            result[name] = operator(val, factor)

    if is_dataclass:
        result = class_(**result)
    elif is_namedtuple:
        result = class_(**result)
    return result


def apply_func_nested(vals, func):
    """Apply function to values in a nested dict or dataclass"""
    is_dataclass = dataclasses.is_dataclass(vals)
    if is_dataclass:
        class_ = type(vals)
        vals = {
            field.name: getattr(vals, field.name) for field in dataclasses.fields(vals)
        }

    result = {}
    for key, val in vals.items():
        if isinstance(val, dict) or dataclasses.is_dataclass(val):
            result[key] = apply_func_nested(val, func)
        else:
            result[key] = func(val)
    if is_dataclass:
        result = class_(**result)
    return result
