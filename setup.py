from setuptools import setup

setup(
    name="nesttool",
    version="0.1.0",
    description="Utility functions for working with nested dicts and data classes",
    url="https://gitlab.com/astoeriko/nesttool",
    author="Anna Störiko",
    author_email="anna.stoeriko@gmx.de",
    license="MIT",
    packages=["nesttool"],
    install_requires=[
        "sunode",
    ],
)
