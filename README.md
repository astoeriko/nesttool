# Nesttool
This package is a small collection of functions to handle nested dicitonaries
and dataclasses in Python.

# Installation
You can install the package using `pip` with
```
pip install git+https://gitlab.com/astoeriko/nesttool.git
```
or clone the repository and install in editable mode with 
```
cd nesttool
pip install -e .
```
